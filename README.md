# Charging

Represents a store for car charging session entities. It holds all records in memory and provide REST API.
Each entity of the store represents unique charging session that can be in progress or finished. 

The purpose of this restful web service is to share the api to operate on these charging sessions, get instances 
and summary.

## Prerequisite

The project is built with Java 8 and would require Java installed with JAVA_HOME pointing towards the correct
installation path.

The project runs on port 8080 and requires that port to be free.

## Installation

The project comes with its embedded Tomcat and platform dependant maven executables.
It does not require anything on top of what it comes with.

To start the project, navigate to the project root and simply run the command:

```
MacOS:
./mvnw spring-boot:run
```

* If you have a special settings.xml file that stops you from fetching the project dependencies,
please use the empty settings.xml file that comes with the project:

```
MacOS:
./mvnw spring-boot:run -s settings.xml
```

* Similarly for windows:
```
Windows:
mvnw.cmd spring-boot:run
```

### Running the tests

The project contains two test suites one for the api contract and one for the session manager holding the 
bulk of business logic.

To run the tests:

```
MacOS:
./mvnw test
```

```
Windows:
mvnw.cmd test
```

### Testing the api

I have included a postman collection to make it easier for api testing. Can be found under /postman directory. 

### Final Notes

- The example document on GET /chargingSessions endpoint has a session with IN_PROGRESS state and a dedicated stoppedAt
value. I assume since it is not stopped, sharing this information is not possible and followed that assumption
in my submission.

- Time is calculated in accordance with the europe time zone. If we know the location of the stations can further 
share the localized time in the api.
