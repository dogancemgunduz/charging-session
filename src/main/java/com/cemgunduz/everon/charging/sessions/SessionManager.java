package com.cemgunduz.everon.charging.sessions;

import com.cemgunduz.everon.charging.sessions.model.Session;
import com.cemgunduz.everon.charging.sessions.model.Status;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * Component in charge of handling storage and retrieval of charging sessions entity
 *
 * Uses a timeseriesDatabase to store all events later to be queried by their occurence timeline,
 * Uses a secondary index to store the final state of events which is indexed by their uuid to further
 * meet the finalize by uuid requirements
 */
@Component
public class SessionManager {

    private static final int ONE = 1;
    private static final UUID SECRET_GENERATION_UUID = UUID.randomUUID();

    private ConcurrentSkipListMap<TimeseriesSessionKey, Session> timeseriesDatabase = new ConcurrentSkipListMap<>();
    private Map<UUID, Session> secondaryIndex = new ConcurrentHashMap<>();

    /**
     * Saves the session to the databases
     * @param session Represents a charging session instance
     */
    public Session save(Session session) {
        timeseriesDatabase.put(generateKey(session), session);
        secondaryIndex.put(session.getId(), session);

        return session;
    }

    /**
     * Finds all persisted charging sessions, since we are interested in the final state the secondary index is used
     * to fetch the entity list
     * @return a collection of all charging session instances
     */
    public Collection<Session> findAll() {
        return secondaryIndex.values();
    }

    /**
     * Creates the summary of all charging sessions for the last minute
     * @return a summary that contains total, in progress and completed charging sessions
     */
    public Collection<Session> summarize() {
        return summarize(ZonedDateTime.now().minusMinutes(ONE));
    }

    /**
     * Creates the summary of all charging sessions
     * @param zonedDateTime time in which summary is going to be made in
     * @return a summary that contains total, in progress and completed charging sessions
     */
    public Collection<Session> summarize(ZonedDateTime zonedDateTime) {
        return timeseriesDatabase.tailMap(
                new TimeseriesSessionKey(
                        SECRET_GENERATION_UUID,
                        zonedDateTime
                )
        ).values();
    }

    /**
     * Fetches the charging session by uuid,
     * Since the primary database is timeseries based uses a secondary index to lookup the uuid key and uses
     * that to fetch the charging session entity. After finalizing persists the finished session.
     * Skips the sessions that are already finalized
     * @param id UUID representing the charging session instance
     * @return the charging session instance, if it does not exist returns null instead
     */
    public Session finish(UUID id) {
        if(!secondaryIndex.containsKey(id)) {
            return null;
        }

        Session olderSession = secondaryIndex.get(id);
        if(olderSession.getStatus().equals(Status.FINISHED)){
            return olderSession;
        }

        Session finishedSession = secondaryIndex.get(id).finishSession();
        save(finishedSession);

        return finishedSession;
    }

    private TimeseriesSessionKey generateKey(Session session) {
        return new TimeseriesSessionKey(
                session.getId(),
                session.getStatus() == Status.IN_PROGRESS ?
                        session.getStartedAt() :
                        session.getStoppedAt()
        );
    }
}
