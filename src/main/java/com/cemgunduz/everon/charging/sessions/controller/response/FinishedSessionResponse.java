package com.cemgunduz.everon.charging.sessions.controller.response;

import com.cemgunduz.everon.charging.sessions.model.Session;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;

import java.time.LocalDateTime;

@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class FinishedSessionResponse extends SessionResponse {

    private final LocalDateTime stoppedAt;

    public FinishedSessionResponse(Session session) {
        super(session);
        this.stoppedAt = super.formatDate(session.getStoppedAt());
    }
}
