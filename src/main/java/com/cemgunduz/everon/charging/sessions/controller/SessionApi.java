package com.cemgunduz.everon.charging.sessions.controller;

import com.cemgunduz.everon.charging.sessions.SessionManager;
import com.cemgunduz.everon.charging.sessions.controller.exceptions.UnknownSessionException;
import com.cemgunduz.everon.charging.sessions.controller.request.StartSessionRequest;
import com.cemgunduz.everon.charging.sessions.controller.response.FinishedSessionResponse;
import com.cemgunduz.everon.charging.sessions.controller.response.SessionResponse;
import com.cemgunduz.everon.charging.sessions.controller.response.SummaryResponse;
import com.cemgunduz.everon.charging.sessions.model.Session;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.cemgunduz.everon.charging.sessions.controller.ApiConstants.Properties.DEFUALT_MEDIA_TYPE;
import static com.cemgunduz.everon.charging.sessions.controller.ApiConstants.URL.CONTEXT_ROOT;
import static com.cemgunduz.everon.charging.sessions.controller.ApiConstants.URL.FINISH_PATH_VARIABLE;
import static com.cemgunduz.everon.charging.sessions.controller.ApiConstants.URL.SUMMARY_PATH;

/**
 * Rest entry point for the charging session related operations
 */
@RestController
@RequestMapping(CONTEXT_ROOT)
public class SessionApi {

    public SessionApi(SessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }

    private final SessionManager sessionManager;

    /**
     * Starts a new charging session with the given station id.
     *
     * @param startSessionRequest Contains the station id which is used to create a new charging session.
     * @return all the details regarding a charging session with an in progress status.
     */
    @PostMapping(consumes = DEFUALT_MEDIA_TYPE, produces = DEFUALT_MEDIA_TYPE)
    public SessionResponse startSession(@RequestBody StartSessionRequest startSessionRequest) {
        Session session = sessionManager.save(
                new Session(startSessionRequest.getStationId())
        );

        return new SessionResponse(session);
    }

    /**
     * Finishes a previously started charging session, if the given session exists, else throws a 404 not found.
     *
     * @param sessionUUID Dedicated unique id associated with the charging session.
     * @return all the details regarding a charging session with a finished status, also contains the stoppedAt time.
     */
    @PutMapping(value = FINISH_PATH_VARIABLE, produces = DEFUALT_MEDIA_TYPE)
    public FinishedSessionResponse finishSessionByUUID(@PathVariable UUID sessionUUID) {
        Session session = sessionManager.finish(sessionUUID);
        if(session == null) {
            throw new UnknownSessionException(sessionUUID.toString());
        }

        return new FinishedSessionResponse(session);
    }

    /**
     * Fetches all the charging sessions, contains both finished and unfinished sessions.
     *
     * @return a list of charging sessions
     */
    @GetMapping(produces = DEFUALT_MEDIA_TYPE)
    public List<SessionResponse> getAllSessions() {
        return sessionManager.findAll().stream()
                .map(session -> new FinishedSessionResponse(session))
                .collect(Collectors.toList());
    }

    /**
     * Fetches the summary information for all charging sessions.
     *
     * @return summary related information that contains total, in progress and finalized charging session amounts.
     */
    @GetMapping(value = SUMMARY_PATH, produces = DEFUALT_MEDIA_TYPE)
    public SummaryResponse getSummary() {
        return new SummaryResponse(
                sessionManager.summarize()
        );
    }
}
