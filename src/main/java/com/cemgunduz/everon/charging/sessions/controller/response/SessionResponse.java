package com.cemgunduz.everon.charging.sessions.controller.response;

import com.cemgunduz.everon.charging.sessions.model.Session;
import com.cemgunduz.everon.charging.sessions.model.Status;
import lombok.Getter;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.UUID;

@Getter
public class SessionResponse {

    private UUID id;
    private String stationId;
    private LocalDateTime startedAt;
    private Status status;

    public SessionResponse(Session session) {
        this.id = session.getId();
        this.stationId = session.getStationId();
        this.startedAt = formatDate(session.getStartedAt());
        this.status = session.getStatus();
    }

    protected LocalDateTime formatDate(ZonedDateTime zonedDateTime) {
        if(zonedDateTime == null) {
            return null;
        }

        return zonedDateTime.toLocalDateTime();
    }
}
