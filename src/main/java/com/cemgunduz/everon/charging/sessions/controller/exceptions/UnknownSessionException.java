package com.cemgunduz.everon.charging.sessions.controller.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class UnknownSessionException extends RuntimeException {

    public UnknownSessionException(String matchId) {
        super("The charging session with the given uuid does not exist: " + matchId);
    }
}
