package com.cemgunduz.everon.charging.sessions.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.UUID;

@Getter
@EqualsAndHashCode
@RequiredArgsConstructor
public class Session {

    private static final ZoneId defaultZoneId = ZoneId.of("Europe/Paris");

    public Session(String stationId) {
        this.stationId = stationId;
        id = UUID.randomUUID();
        startedAt = ZonedDateTime.now(defaultZoneId);
        stoppedAt = null;
        status = Status.IN_PROGRESS;
    }

    private final UUID id;
    private final String stationId;
    private final ZonedDateTime startedAt;
    private final ZonedDateTime stoppedAt;
    private final Status status;

    public Session finishSession() {
        return new Session(
                id,
                stationId,
                startedAt,
                ZonedDateTime.now(defaultZoneId),
                Status.FINISHED
        );
    }
}
