package com.cemgunduz.everon.charging.sessions.model;

public enum Status {
    IN_PROGRESS, FINISHED
}
