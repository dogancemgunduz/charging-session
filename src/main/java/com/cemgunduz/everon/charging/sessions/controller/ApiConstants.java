package com.cemgunduz.everon.charging.sessions.controller;

public final class ApiConstants {

    public static final class URL {
        public static final String CONTEXT_ROOT = "chargingSessions";
        public static final String SUMMARY_PATH = "/summary";
        public static final String FINISH_PATH_VARIABLE = "/{sessionUUID}";
    }

    public static final class Properties {
        public static final String DEFUALT_MEDIA_TYPE = "application/json";
    }

}
