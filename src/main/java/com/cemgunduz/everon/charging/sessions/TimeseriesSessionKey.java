package com.cemgunduz.everon.charging.sessions;

import lombok.Data;

import java.time.ZonedDateTime;
import java.util.UUID;

@Data
public class TimeseriesSessionKey implements Comparable<TimeseriesSessionKey> {

    private final UUID sessionUUID;
    private final ZonedDateTime timeOfInterest;

    @Override
    public int compareTo(TimeseriesSessionKey o) {
        long result = this.timeOfInterest.toInstant().toEpochMilli() - o.getTimeOfInterest().toInstant().toEpochMilli();

        if(result == 0){
            return this.sessionUUID.compareTo(o.sessionUUID);
        }

        return result > 0 ? 1 : -1;
    }
}
