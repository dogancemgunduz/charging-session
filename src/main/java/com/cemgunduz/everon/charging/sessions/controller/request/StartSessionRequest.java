package com.cemgunduz.everon.charging.sessions.controller.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class StartSessionRequest {

    private String stationId;
}
