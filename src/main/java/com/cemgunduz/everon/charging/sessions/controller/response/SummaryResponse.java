package com.cemgunduz.everon.charging.sessions.controller.response;

import com.cemgunduz.everon.charging.sessions.model.Session;
import com.cemgunduz.everon.charging.sessions.model.Status;
import lombok.Getter;

import java.util.Collection;

@Getter
public class SummaryResponse {

    private int totalCount;
    private int startedCount;
    private int stoppedCount;

    public SummaryResponse(Collection<Session> sessions) {
        this.totalCount = sessions.size();
        this.startedCount = (int) sessions.stream()
                .filter(session -> session.getStatus().equals(Status.IN_PROGRESS))
                .count();
        this.stoppedCount = totalCount - startedCount;
    }
}
