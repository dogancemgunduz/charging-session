package com.cemgunduz.everon.charging;

import com.cemgunduz.everon.charging.sessions.SessionManager;
import com.cemgunduz.everon.charging.sessions.model.Session;
import com.cemgunduz.everon.charging.sessions.model.Status;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

import java.time.ZonedDateTime;
import java.util.UUID;

public class SessionManagerTest {

    private SessionManager sessionManager;

    @BeforeEach
    public void initialize() {
        sessionManager = new SessionManager();
    }

    @Test
    public void canSaveSession() {
        Assert.isTrue(
                sessionManager.findAll().size() == 0,
                "The database is empty"
        );

        Session session = new Session("ABC");
        sessionManager.save(session);

        Assert.isTrue(
                sessionManager.findAll().size() == 1,
                "The database contains one entry"
        );

        Assert.isTrue(
                sessionManager.findAll().stream().findFirst().get() == session,
                "The entry equals to our session"
        );
    }

    @Test
    public void canFindAllSessions() {
        Assert.isTrue(
                sessionManager.findAll().size() == 0,
                "The database is empty"
        );

        Session session1 = new Session("ABC-1");
        Session session2 = new Session("ABC-2");
        Session session3 = new Session("ABC-3");

        sessionManager.save(session1);
        sessionManager.save(session2);
        sessionManager.save(session3);

        Assert.isTrue(
                sessionManager.findAll().size() == 3,
                "The database contains three entries"
        );

        Assert.isTrue(
                sessionManager.findAll().contains(session1),
                "The database contains the entry 1"
        );

        Assert.isTrue(
                sessionManager.findAll().contains(session2),
                "The database contains the entry 2"
        );

        Assert.isTrue(
                sessionManager.findAll().contains(session3),
                "The database contains the entry 3"
        );
    }

    @Test
    public void canFilterSessionsByTime() throws InterruptedException {
        Session session = new Session("TEST");
        sessionManager.save(session);

        Assert.isTrue(
                sessionManager.summarize().contains(session),
                "Our charging session is part of the summary report"
        );

        Thread.sleep(1000L);

        Assert.isTrue(
                !sessionManager.summarize(ZonedDateTime.now().minusSeconds(1)).contains(session),
                "Our charging session is no longer part of the summary report"
        );
    }

    @Test
    public void canSaveTwoSessionsWithTheSameTime() {
        Session session = new Session("TEST");
        sessionManager.save(session);

        Session session2 = new Session(
                UUID.randomUUID(),
                "TEST-STATION",
                session.getStartedAt(), // When using the same time, entity is not overriden
                ZonedDateTime.now(),
                Status.IN_PROGRESS
        );

        sessionManager.save(session2);

        Assert.isTrue(
                sessionManager.summarize().size() == 2,
                "A seperate entry with the same date time can be added to the timeseries database"
        );
    }

    @Test
    public void canFinishSessions() {
        Session session = new Session("TEST");
        sessionManager.save(session);
        Session finishedSession = sessionManager.finish(session.getId());

        Assert.isTrue(
                sessionManager.findAll().size() == 1,
                "The database contains one entry"
        );

        Assert.isTrue(
                sessionManager.findAll().stream().findFirst().get() == finishedSession,
                "The entry equals to our finalized session"
        );

        Assert.isTrue(
                sessionManager.summarize().size() == 2,
                "The timeseries database contains both statuses"
        );

        Assert.isTrue(
                sessionManager.summarize().contains(session),
                "The timeseries database contains in progress status"
        );

        Assert.isTrue(
                sessionManager.summarize().contains(finishedSession),
                "The timeseries database contains finished status"
        );
    }

    @Test
    public void canFinishWhenSessionDoesNotExist() {
        Assert.isNull(
                sessionManager.finish(UUID.randomUUID())  ,
                "Returned null shen session deos not exist"
        );
    }

    @Test
    public void canSkipWhenSessionAlreadyFinished() {
        Session session = new Session("TEST");
        sessionManager.save(session);
        Session finalizedSession = sessionManager.finish(session.getId());

        Assert.isTrue(
                sessionManager.finish(session.getId()) == finalizedSession ,
                "Did not create a new finalized session, instead returned previously finalized session"
        );
    }
}
