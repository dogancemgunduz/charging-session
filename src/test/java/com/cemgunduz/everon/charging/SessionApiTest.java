package com.cemgunduz.everon.charging;

import com.cemgunduz.everon.charging.sessions.SessionManager;
import com.cemgunduz.everon.charging.sessions.controller.request.StartSessionRequest;
import com.cemgunduz.everon.charging.sessions.model.Session;
import com.cemgunduz.everon.charging.sessions.model.Status;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.UUID;

import static com.cemgunduz.everon.charging.sessions.controller.ApiConstants.URL.CONTEXT_ROOT;
import static com.cemgunduz.everon.charging.sessions.controller.ApiConstants.URL.SUMMARY_PATH;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SessionApiTest {

    @MockBean
    private SessionManager sessionManager;

    @Autowired
    private TestRestTemplate restTemplate;

    private Session testSessionInProgress = new Session(
            UUID.fromString("ae60918f-7871-49c2-9767-033e12f06278"),
            "TEST_SESSION_1",
            ZonedDateTime.ofInstant(Instant.EPOCH.plus(49, ChronoUnit.DAYS), ZoneId.of("Europe/Paris")),
            null,
            Status.IN_PROGRESS
    );

    private Session testSessionFinished = new Session(
            UUID.fromString("ad5b8725-a269-4f57-802c-46e7585f4b37"),
            "TEST_SESSION_2",
            ZonedDateTime.ofInstant(Instant.EPOCH.plus(50, ChronoUnit.DAYS), ZoneId.of("Europe/Paris")),
            ZonedDateTime.ofInstant(Instant.EPOCH.plus(51, ChronoUnit.DAYS), ZoneId.of("Europe/Paris")),
            Status.FINISHED
    );

    @Test
    public void canStartSession() {
        // given
        given(sessionManager.save(any()))
                .willReturn(testSessionInProgress);

        // when
        ResponseEntity<String> httpResponse = restTemplate.postForEntity(
                "/" + CONTEXT_ROOT,
                new StartSessionRequest(testSessionInProgress.getStationId()),
                String.class
        );

        // then
        Assert.isTrue(
                httpResponse.getStatusCode().is2xxSuccessful(),
                "The http response is 200 OK"
        );

        String expectedJson = "{" +
                "\"id\":\"ae60918f-7871-49c2-9767-033e12f06278\"," +
                "\"stationId\":\"TEST_SESSION_1\"," +
                "\"startedAt\":\"1970-02-19T01:00:00\"," +
                "\"status\":\"IN_PROGRESS\"}";

        Assert.isTrue(
                httpResponse.getBody().equals(expectedJson) ,
                "Finished session is successfully returned back"
        );
    }

    @Test
    public void canFinishSessionById() {
        // given
        given(sessionManager.finish(testSessionFinished.getId()))
                .willReturn(testSessionFinished);

        // when
        ResponseEntity<String> httpResponse = restTemplate.exchange(
                "/" + CONTEXT_ROOT + "/" + testSessionFinished.getId().toString(),
                HttpMethod.PUT,
                null,
                String.class);

        // then
        Assert.isTrue(
                httpResponse.getStatusCode().is2xxSuccessful(),
                "The http response is 200 OK"
        );

        String expectedJson = "{" +
                "\"id\":\"ad5b8725-a269-4f57-802c-46e7585f4b37\"," +
                "\"stationId\":\"TEST_SESSION_2\"," +
                "\"startedAt\":\"1970-02-20T01:00:00\"," +
                "\"status\":\"FINISHED\"," +
                "\"stoppedAt\":\"1970-02-21T01:00:00\"}";

        Assert.isTrue(
                httpResponse.getBody().equals(expectedJson) ,
                "Finished session is successfully returned back"
        );
    }

    @Test
    public void canThrow404IfSessionNotFound() {
        // given
        given(sessionManager.finish(any()))
                .willReturn(null);

        // when
        ResponseEntity<String> httpResponse = restTemplate.exchange(
                "/" + CONTEXT_ROOT + "/" + testSessionFinished.getId().toString(),
                HttpMethod.PUT,
                null,
                String.class);

        // then
        Assert.isTrue(
                httpResponse.getStatusCode().value() == 404,
                "The http response is 404 Not Found"
        );
    }

    @Test
    public void canSummarizeSession() {
        // given
        given(sessionManager.summarize())
                .willReturn(
                        Arrays.asList(
                                testSessionInProgress,
                                testSessionFinished
                        )
                );

        // when
        ResponseEntity<String> httpResponse = restTemplate.getForEntity(
                "/" + CONTEXT_ROOT + "/" + SUMMARY_PATH,
                String.class
        );

        // then
        Assert.isTrue(
                httpResponse.getStatusCode().is2xxSuccessful(),
                "The http response is 200 OK"
        );

        String expectedJson = "{" +
                "\"totalCount\":2," +
                "\"startedCount\":1," +
                "\"stoppedCount\":1}";

        Assert.isTrue(
                httpResponse.getBody().equals(expectedJson) ,
                "Finished session is successfully returned back"
        );
    }

    @Test
    public void canFindAllSessions() {
        // given
        given(sessionManager.findAll())
                .willReturn(
                        Arrays.asList(
                                testSessionInProgress,
                                testSessionFinished
                        )
                );

        // when
        ResponseEntity<String> httpResponse = restTemplate.getForEntity(
                "/chargingSessions",
                String.class
        );

        // then
        Assert.isTrue(
                httpResponse.getStatusCode().is2xxSuccessful(),
                "The http response is 200 OK"
        );

        String expectedJson = "[{" +
                "\"id\":\"ae60918f-7871-49c2-9767-033e12f06278\"," +
                "\"stationId\":\"TEST_SESSION_1\"," +
                "\"startedAt\":\"1970-02-19T01:00:00\"," +
                "\"status\":\"IN_PROGRESS\",\"stoppedAt\":null}," +
                "{\"id\":\"ad5b8725-a269-4f57-802c-46e7585f4b37\"," +
                "\"stationId\":\"TEST_SESSION_2\"," +
                "\"startedAt\":\"1970-02-20T01:00:00\"," +
                "\"status\":\"FINISHED\"," +
                "\"stoppedAt\":\"1970-02-21T01:00:00\"}]";

        Assert.isTrue(
                httpResponse.getBody().equals(expectedJson),
                "Both charging sessions are returned correctly"
        );
    }
}
